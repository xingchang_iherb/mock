var http = require('http');
var mockserver = require('mockserver');
var open = require('open');
var port = 666;

http.createServer(mockserver('./mocks')).listen(port, () => {
    open(`http://127.0.0.1:${port}`);
});
