# BackOffice Core Mock API

  
## How to run locally

`node ./server.js`


## CORS support
Add `access-control-allow-origin: *` into your response header

## Authorization in header
Add `access-control-allow-headers: Authorization` into your response header